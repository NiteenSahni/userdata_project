let problem5 = (users) => {
    if (users.length == 0 || Array.isArray(users) == false) {
        return []
    }

    let maleUsers = []
    let userIndex = 0;

    for (let index = 0; index < users.length; index++) {
        if (users[index].gender == "Male") {

            maleUsers[userIndex] = []
            maleUsers[userIndex].push(users[index].first_name + " " + users[index].last_name, users[index].email)
            userIndex = userIndex + 1;
        }
    }
    console.log("the number of Male Users are " + maleUsers.length)
    return maleUsers
}
module.exports = problem5;
