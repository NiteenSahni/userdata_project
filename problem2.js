let problem2 = (users) => {

    if (users.length == 0 || Array.isArray(users) == false) {
        return []
    }
    let lastUser
    lastUser = "Last user is " + users[users.length - 1].first_name + " " + users[users.length - 1].last_name + " and can be contacted on " + users[users.length - 1].email
    console.log(lastUser)
    return users[users.length - 1];
}
module.exports = problem2
