let problem6 = (users) => {
    if (users.length == 0 || Array.isArray(users) == false) {
        return []
    }
    let male = []
    let female = []
    let polygender = []
    let bigender = []
    let genderqueer = []
    let genderfluid = []
    let agender = []
    let nonBinay = []


    for (let index = 0; index < users.length; index++) {
        if (users[index].gender == "Male") {
            male.push(users[index])
        }
        if (users[index].gender == "Female") {
            female.push(users[index])
        }
        if (users[index].gender == "Polygender") {
            polygender.push(users[index])
        }
        if (users[index].gender == "Bigender") {
            bigender.push(users[index])
        }
        if (users[index].gender == "Genderqueer") {
            genderqueer.push(users[index])
        }
        if (users[index].gender == "Genderfluid") {
            genderfluid.push(users[index])
        }
        if (users[index].gender == "Agender") {
            agender.push(users[index])
        }
        if (users[index].gender == "Non-binary") {
            agender.push(users[index])
        }


    }


    return JSON.stringify([male, female, polygender, bigender, genderfluid, agender, nonBinay]);


}
module.exports = problem6