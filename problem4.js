let problem4 = (users) => {
    if (users.length == 0 || Array.isArray(users) == false) {
        return []
    }
    let emailArray = []

    for (let index = 0; index < users.length; index++) {
        emailArray.push(users[index].email)
    }

    emailArray.sort()
    return emailArray;
}

module.exports = problem4;
